//
//  MovieCellViewModelTests.swift
//  PremierSwiftTests
//
//  Created by Andy on 27/06/2019.
//  Copyright © 2019 Pyrotechnic Apps. All rights reserved.
//

import XCTest
@testable import Movies

class MovieCellViewModelTests: XCTestCase {

    let movieRecord1json = #"{"vote_count":2068,"id":19404,"video":false,"vote_average":9,"title":"Dilwale Dulhania Le Jayenge","popularity":19.434,"poster_path":"/uC6TTUhPpQCmgldGyYveKRAu8JN.jpg","original_language":"hi","original_title":"दिलवाले दुल्हनिया ले जायेंगे","genre_ids":[35,18,10749],"backdrop_path":"/nl79FQ8xWZkhL3rDr1v2RFFR6J0.jpg","adult":false,"overview":"Raj is a rich, carefree, happy-go-lucky second generation NRI. Simran is the daughter of Chaudhary Baldev Singh, who in spite of being an NRI is very strict about adherence to Indian values. Simran has left for India to be married to her childhood fiancé. Raj leaves for India with a mission at his hands, to claim his lady love under the noses of her whole family. Thus begins a saga.","release_date":"1995-10-20"}"#.data(using: .utf8)!

    var movie1: Movie!
    var movieCellViewModel: MovieCellViewModel!
    let imageCache = ImageCache()

    override func setUp() {
        do {
            let jsonDecoder = JSONDecoder()
            movie1 =  try jsonDecoder.decode(Movie.self, from: movieRecord1json)
            movieCellViewModel = MovieCellViewModel (movie: movie1, movieIsFavourite: false, imageCache: imageCache)
        }
        catch {
            XCTFail("ERROR: Failure for a movie record \(error)")
        }
    }

    override func tearDown() {
        movie1 = nil
    }

    func testImageFullPath() {
        XCTAssertEqual(movieCellViewModel.imageFullPath , "https://image.tmdb.org/t/p/w500/uC6TTUhPpQCmgldGyYveKRAu8JN.jpg")
    }

    func testOverview() {
        XCTAssertEqual(movieCellViewModel.overview, "Raj is a rich, carefree, happy-go-lucky second generation NRI. Simran is the daughter of Chaudhary Baldev Singh, who in spite of being an NRI is very strict about adherence to Indian values. Simran has left for India to be married to her childhood fiancé. Raj leaves for India with a mission at his hands, to claim his lady love under the noses of her whole family. Thus begins a saga.")
    }

    func testTitle() {
        XCTAssertEqual(movieCellViewModel.title, "Dilwale Dulhania Le Jayenge")
    }

}
