//
//  APIEndPointTests.swift
//  PremierSwiftTests
//
//  Created by Andy on 26/06/2019.
//  Copyright © 2019 Pyrotechnic Apps. All rights reserved.
//

import XCTest
@testable import Movies

class APIEndPointTests: XCTestCase {

    let topMoviesEndPoint = APIEndpoint.topRatedMovies(apiKey: "e4f9e61f6ffd66639d33d3dde7e3159b")
    let imageEndPoint = APIEndpoint.images(imagePath: "/7IiTTgloJzvGI1TAYymCfbfl3vT.jpg")

    override func setUp() {
    }

    override func tearDown() {
    }

    func testEndPoint_Scheme() {
        XCTAssertEqual(topMoviesEndPoint.scheme, "https")
    }

    func testEndPoint_Host() {
        XCTAssertEqual(topMoviesEndPoint.host, "api.themoviedb.org")
    }

    func testEndPoint_HttpMethod() {
        XCTAssertEqual(topMoviesEndPoint.httpMethod, "GET")
    }

    func testEndPoint_ContentType() {
        XCTAssertEqual(topMoviesEndPoint.contentType, "application/json")
    }

    func testEndPoint_path() {
        XCTAssertEqual(topMoviesEndPoint.path, "/3/movie/top_rated")
    }

    func testImageEndPoint_fullPath() {
        XCTAssertEqual(imageEndPoint.fullPath, "https://image.tmdb.org/t/p/w500/7IiTTgloJzvGI1TAYymCfbfl3vT.jpg")
    }

    func testEndPoint_response() {
        XCTAssertNotNil(topMoviesEndPoint.request?.url)
        XCTAssertEqual(topMoviesEndPoint.request?.url?.description, "https://api.themoviedb.org/3/movie/top_rated?api_key=e4f9e61f6ffd66639d33d3dde7e3159b")
    }

}
