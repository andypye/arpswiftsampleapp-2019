//
//  FavouriteMoviesTest.swift
//  PremierSwift2Tests
//
//  Created by Andy on 30/06/2019.
//  Copyright © 2019 Pyrotechnic Apps. All rights reserved.
//

import XCTest
@testable import Movies

class FavouriteMoviesTest: XCTestCase {

    var sut: Favourites!
    var movie1: Movie!
    var movie2: Movie!
    var movie3: Movie!

    let movieRecord1json = #"{"vote_count":2068,"id":19404,"video":false,"vote_average":9,"title":"Dilwale Dulhania Le Jayenge","popularity":19.434,"poster_path":"/uC6TTUhPpQCmgldGyYveKRAu8JN.jpg","original_language":"hi","original_title":"दिलवाले दुल्हनिया ले जायेंगे","genre_ids":[35,18,10749],"backdrop_path":"/nl79FQ8xWZkhL3rDr1v2RFFR6J0.jpg","adult":false,"overview":"Raj is a rich, carefree, happy-go-lucky second generation NRI. Simran is the daughter of Chaudhary Baldev Singh, who in spite of being an NRI is very strict about adherence to Indian values. Simran has left for India to be married to her childhood fiancé. Raj leaves for India with a mission at his hands, to claim his lady love under the noses of her whole family. Thus begins a saga.","release_date":"1995-10-20"}"#.data(using: .utf8)!
    let movieRecord2json = #"{"vote_count":179,"id":496243,"video":false,"vote_average":8.9,"title":"Parasite","popularity":31.262,"poster_path":"/7IiTTgloJzvGI1TAYymCfbfl3vT.jpg","original_language":"ko","original_title":"기생충","genre_ids":[18,53],"backdrop_path":"/ny5aCtglk2kceGAuAdiyqbhBBAf.jpg","adult":false,"overview":"All unemployed, Ki-taek's family takes peculiar interest in the wealthy and glamorous Parks for their livelihood until they get entangled in an unexpected incident.","release_date":"2019-05-30"}"#.data(using: .utf8)!
    let movieRecord3json = #"{"vote_count":13261,"id":278,"video":false,"vote_average":8.7,"title":"The Shawshank Redemption","popularity":38.716,"poster_path":"/9O7gLzmreU0nGkIB6K3BsJbzvNv.jpg","original_language":"en","original_title":"The Shawshank Redemption","genre_ids":[18,80],"backdrop_path":"/j9XKiZrVeViAixVRzCta7h1VU9W.jpg","adult":false,"overview":"Framed in the 1940s for the double murder of his wife and her lover, upstanding banker Andy Dufresne begins a new life at the Shawshank prison, where he puts his accounting skills to work for an amoral warden. During his long stretch in prison, Dufresne comes to be admired by the other inmates -- including an older prisoner named Red -- for his integrity and unquenchable sense of hope.","release_date":"1994-09-23"}"#.data(using: .utf8)!

    override func setUp() {
        sut = Favourites()
        sut.clearFavouriteMovies()
        do {
            let jsonDecoder = JSONDecoder()
            movie1 =  try jsonDecoder.decode(Movie.self, from: movieRecord1json)
            movie2 =  try jsonDecoder.decode(Movie.self, from: movieRecord2json)
            movie3 =  try jsonDecoder.decode(Movie.self, from: movieRecord3json)
        }
        catch {
            XCTFail("ERROR: Failure for a movie record \(error)")
        }
    }

    override func tearDown() {
        sut.clearFavouriteMovies()
        sut = nil
        movie1 = nil
        movie2 = nil
        movie3 = nil
    }

    func populateMovies() {
        sut.add(movieId: movie1.movieId)
        sut.add(movieId: movie2.movieId)
        sut.add(movieId: movie3.movieId)
    }

    func testAddOneMovie() {
        sut.add(movieId: movie1.movieId)
        XCTAssertEqual(sut.movies.count, 1)
    }

    func testpopulateMovies() {
        populateMovies()
        XCTAssertEqual(sut.movies.count, 3)
        XCTAssertEqual(sut?.movies[0], movie1.movieId)
        XCTAssertEqual(sut?.movies[1], movie2.movieId)
        XCTAssertEqual(sut?.movies[2], movie3.movieId)
    }

    func testRemove() {
        populateMovies()
        sut.remove(movieId: movie2.movieId)
        XCTAssertEqual(sut.movies.count, 2)
        XCTAssertEqual(sut?.movies[0], movie1.movieId)
        XCTAssertEqual(sut?.movies[1], movie3.movieId)
    }

    func testClearFavouriteMovies() {
        populateMovies()
        sut.clearFavouriteMovies()
        XCTAssertEqual(sut.movies.count, 0)
    }

    func testRemove_empty() {
        sut.clearFavouriteMovies()
        // we want it to just cope ok, without error
        XCTAssertNoThrow(sut.remove(movieId: movie2.movieId))
    }

    func testDeduplication() {
        populateMovies()
        populateMovies()
        XCTAssertEqual(sut.movies.count, 3)
    }

}
