//
//  MovieTests.swift
//  PremierSwiftTests
//
//  Created by Andy on 26/06/2019.
//  Copyright © 2019 Pyrotechnic Apps. All rights reserved.
//

import XCTest
@testable import Movies

class MovieTests: XCTestCase {

    let movieRecord1json = #"{"vote_count":2068,"id":19404,"video":false,"vote_average":9,"title":"Dilwale Dulhania Le Jayenge","popularity":19.434,"poster_path":"/uC6TTUhPpQCmgldGyYveKRAu8JN.jpg","original_language":"hi","original_title":"दिलवाले दुल्हनिया ले जायेंगे","genre_ids":[35,18,10749],"backdrop_path":"/nl79FQ8xWZkhL3rDr1v2RFFR6J0.jpg","adult":false,"overview":"Raj is a rich, carefree, happy-go-lucky second generation NRI. Simran is the daughter of Chaudhary Baldev Singh, who in spite of being an NRI is very strict about adherence to Indian values. Simran has left for India to be married to her childhood fiancé. Raj leaves for India with a mission at his hands, to claim his lady love under the noses of her whole family. Thus begins a saga.","release_date":"1995-10-20"}"#.data(using: .utf8)!

    var movie1: Movie!

    override func setUp() {
        do {
            let jsonDecoder = JSONDecoder()
            movie1 =  try jsonDecoder.decode(Movie.self, from: movieRecord1json)
        }
        catch {
            XCTFail("ERROR: Failure for a movie record \(error)")
        }
    }

    override func tearDown() {
        movie1 = nil
    }

    func testMovie_voteCount() {
        XCTAssertEqual(movie1.voteCount, 2068)
    }

    func testMovie_movieID() {
        XCTAssertEqual(movie1.movieId, 19404)
    }

    func testMovie_video() {
        XCTAssertEqual(movie1.video, false)
    }

    func testMovie_voteAverage() {
        XCTAssertEqual(movie1.voteAverage, 9)
    }

    func testMovie_title() {
        XCTAssertEqual(movie1.title, "Dilwale Dulhania Le Jayenge")
    }

    func testMovie_popularity() {
        XCTAssertEqual(movie1.popularity, 19.434)
    }

    func testMovie_posterPath() {
        XCTAssertEqual(movie1.posterPath, "/uC6TTUhPpQCmgldGyYveKRAu8JN.jpg")
    }

    func testMovie_originalLanguage() {
        XCTAssertEqual(movie1.originalLanguage, "hi")
    }

    func testMovie_originalTitle() {
        XCTAssertEqual(movie1.originalTitle, "दिलवाले दुल्हनिया ले जायेंगे")
    }

    func testMovie_genreIds() {
        XCTAssertEqual(movie1.genreIds, [35,18,10749])
    }

    func testMovie_backdropPath() {
        XCTAssertEqual(movie1.backdropPath, "/nl79FQ8xWZkhL3rDr1v2RFFR6J0.jpg")
    }

    func testMovie_adult() {
        XCTAssertEqual(movie1.adult, false)
    }

    func testMovie_overview() {
        XCTAssertEqual(movie1.overview, "Raj is a rich, carefree, happy-go-lucky second generation NRI. Simran is the daughter of Chaudhary Baldev Singh, who in spite of being an NRI is very strict about adherence to Indian values. Simran has left for India to be married to her childhood fiancé. Raj leaves for India with a mission at his hands, to claim his lady love under the noses of her whole family. Thus begins a saga.")
    }

    func testreleaseDate() {
        //XCTAssertEqual(movie1.releaseDate, "1995-10-20")
    }
}
