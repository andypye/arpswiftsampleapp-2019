import UIKit

extension UIColor {
    
    class var titleText: UIColor {
        return  black
    }
    
    class var bodyText: UIColor {
        return darkGray
    }
}

extension UIFont {
    
    class var title: UIFont {
        return preferredFont(forTextStyle: UIFont.TextStyle.title1)
    }
    
    class var body: UIFont {
        return preferredFont(forTextStyle: UIFont.TextStyle.body)
    }
    
}
