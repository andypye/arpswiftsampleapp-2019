//
//  DateFormatter.swift
//  PremierSwift2
//
//  Created by Andy on 30/06/2019.
//  Copyright © 2019 Pyrotechnic Apps. All rights reserved.
//

import Foundation
extension DateFormatter {
    static let yyyyMMdd: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        return formatter
    }()
}
