//
//  UIImageView+API.swift
//  PremierSwift
//
//  Created by Andy on 26/06/2019.
//  Copyright © 2019 Pyrotechnic Apps. All rights reserved.
//

import UIKit

typealias ImageCache = NSCache<AnyObject, UIImage>

extension UIImageView {

    func loadImageUsingUrlString(urlString: String, imageCache: ImageCache) {
        if let imageFromCache = imageCache.object(forKey: urlString as AnyObject) {
            self.image = imageFromCache
            return
        }
        if let url = URL(string: urlString) {
            URLSession.shared.dataTask(with: url) { (data, responses, error) in
                if let unwrappedError = error {
                    print("ERROR: Image download failed: \(unwrappedError)")
                    self.setPlaceHolder()
                    return
                }
                guard let unwrappedData = data else {
                    print("ERROR: No data when getting image, so we leave imageview untouched")
                    self.setPlaceHolder()
                    return
                }
                // If all ok, then update the image, must be on the main thread
                DispatchQueue.main.async {
                    self.updateImageAndCache(newImage: UIImage(data: unwrappedData), urlString: urlString, imageCache: imageCache)
                }
                }.resume()
        }
        else {
            print("ERROR: Invalid URL")
        }
    }

    func updateImageAndCache(newImage: UIImage?, urlString: String, imageCache: ImageCache) {
        if let newImage = newImage {
            self.image = newImage
            imageCache.setObject(newImage, forKey: urlString as AnyObject)
        }
        else {
            print("ERROR: Image was nil, so we leave imageview untouched")
        }
    }

    func setPlaceHolder() {
        if let placeHolderImage = UIImage(named: "Placeholder") {
            DispatchQueue.main.async {
                self.image = placeHolderImage
            }
        }
        else {
            print("ERROR: Placeholder not found")
        }
    }

}
