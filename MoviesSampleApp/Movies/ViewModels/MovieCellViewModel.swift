//
//  MovieCellViewModel.swift
//  PremierSwift
//
//  Created by Andy on 27/06/2019.
//  Copyright © 2019 Pyrotechnic Apps. All rights reserved.
//

import Foundation

public struct MovieCellViewModel {
    let movie: Movie
    let imageCache: ImageCache
    let movieIsFavourite: Bool

    init(movie: Movie, movieIsFavourite: Bool, imageCache: ImageCache) {
        self.movie = movie
        self.imageCache = imageCache
        self.movieIsFavourite = movieIsFavourite
    }

    var title: String {
        return movie.title
    }

    var overview: String {
        return movie.overview
    }

    var isFavourite: Bool {
        return movieIsFavourite
    }

    var imageFullPath: String {
        let endPoint = APIEndpoint.images(imagePath: movie.posterPath)
        return endPoint.fullPath
    }

}
