import UIKit

class MoviesViewController: UITableViewController, MovieDelegate {
    // MARK:- Private Properties
    var movieData: MovieData?
    private let activityIndicator = UIActivityIndicatorView()
    private let imageCache = ImageCache()
    private var viewModelToPass: MovieCellViewModel?
    private var favourites = Favourites()

    // This is overkill for now, but putting in anyway, incase different cell types come in later
    private enum tableViewCells: String {
        case movie
        var reuseIdentifier: String {
            switch self {
            case .movie:
                return "MovieCell"
            }
        }
    }

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupActivityIndicator()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 400
        repopulateTableiew()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // If it's a MovieDetailVC
        let destinationVC = segue.destination as! MovieDetailViewController
        destinationVC.movieViewModel = self.viewModelToPass
        destinationVC.movieDelegate = self
    }

    // MARK: - Private Methods
    private func setupActivityIndicator() {
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.style = .gray
        view.addSubview(activityIndicator)
        activityIndicator.isHidden = false
    }

    private func repopulateTableiew() {
        activityIndicator.startAnimating()
        MoviesAPI.getMovies(completion: { fetchedMovies, success, userMessage in
            print("fetchedMovies \(fetchedMovies?.movies.count ?? 0)")
            self.movieData = fetchedMovies
            // Update the UI once we have the movies
            DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
                if success {
                    self.tableView.reloadData()
                } else {
                    // display some kind of holding page
                    print("No movieData returned")
                }
            }
        })
    }

    // MARK: - UITableViewDataSource
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let movieData = movieData else { return 0 }
        return movieData.movies.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: tableViewCells.movie.reuseIdentifier, for: indexPath) as! MovieCell
        guard let movieData = movieData else { return cell }
        let movie = movieData.movies[indexPath.row]
        let movieCellViewModel = MovieCellViewModel(movie: movie, movieIsFavourite: movieData.favourites.isFavourite(movieId: movie.movieId), imageCache: imageCache)
        cell.configure(withViewModel: movieCellViewModel)
        return cell
    }

    // MARK: - UITableViewDelegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let selectedMovie = movieData?.movies[indexPath.row] {
            let isFavourite = movieData?.favourites.isFavourite(movieId: selectedMovie.movieId) ?? false
            self.viewModelToPass = MovieCellViewModel(movie: selectedMovie, movieIsFavourite: isFavourite, imageCache: imageCache)
            performSegue(withIdentifier: "ShowMovieDetails", sender: self)
        }
        else {
            self.viewModelToPass = nil
        }
    }

    // MARK: - MovieDelegate
    func didAddFavourite(movie: Movie) {
        movieData?.favourites.add(movieId: movie.movieId)
    }

    func didRemoveFavourite(movie: Movie) {
        movieData?.favourites.remove(movieId: movie.movieId)
    }
}
