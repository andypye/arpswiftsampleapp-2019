//
//  MovieDetailViewController.swift
//  PremierSwift2
//
//  Created by Andy on 29/06/2019.
//  Copyright © 2019 Pyrotechnic Apps. All rights reserved.
//

import UIKit

class MovieDetailViewController: UIViewController {

    // MARK:- Outlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var thumbnailImage: UIImageView!
    @IBOutlet weak var overviewLabel: UILabel!
    @IBOutlet weak var favouriteSwitch: UISwitch!

    // MARK: Delegates
    var movieDelegate: MovieDelegate!

    // MARK:- Private Properties
    var movieViewModel: MovieCellViewModel!

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configure(withViewModel: self.movieViewModel)
    } 

    // MARK: - Private Methods
    func configure(withViewModel viewModel: MovieCellViewModel) {
        titleLabel.text = viewModel.title
        overviewLabel.text = viewModel.overview
        titleLabel.textColor = .titleText
        titleLabel.font = .title
        overviewLabel.textColor = .bodyText
        overviewLabel.font = .body
        thumbnailImage.loadImageUsingUrlString(urlString: viewModel.imageFullPath
            ,imageCache: viewModel.imageCache)
        favouriteSwitch.isOn = viewModel.movieIsFavourite
    }

    // MARK: - IBActions
    @IBAction func saveFavourite(_ sender: Any) {
        let movie = movieViewModel.movie
        if favouriteSwitch.isOn {
            movieDelegate.didAddFavourite(movie: movie)
        }
        else {
            movieDelegate.didRemoveFavourite(movie: movie)
        }
    }

}
