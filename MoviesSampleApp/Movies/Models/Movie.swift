//
//  Movie.swift
//  PremierSwift
//
//  Created by Andy on 26/06/2019.
//  Copyright © 2019 Pyrotechnic Apps. All rights reserved.
//

import Foundation

typealias MovieId =  Int

struct Movie: Decodable, Hashable {
    let voteCount: Int
    let movieId: MovieId
    let video: Bool
    let voteAverage: Double
    let title: String
    let popularity: Double
    let posterPath: String
    let originalLanguage: String
    let originalTitle: String
    let genreIds: [Int]
    let backdropPath: String
    let adult: Bool
    let overview: String
    let releaseDate: String

    private enum CodingKeys: String, CodingKey {
    case voteCount = "vote_count"
    case movieId = "id"
    case video = "video"
    case voteAverage = "vote_average"
    case title = "title"
    case popularity = "popularity"
    case posterPath = "poster_path"
    case originalLanguage = "original_language"
    case originalTitle = "original_title"
    case genreIds = "genre_ids"
    case backdropPath = "backdrop_path"
    case adult = "adult"
    case overview = "overview"
    case releaseDate = "release_date"
    }

}
