//
//  FavouriteMovies.swift
//  PremierSwift2
//
//  Created by Andy on 30/06/2019.
//  Copyright © 2019 Pyrotechnic Apps. All rights reserved.
//

import Foundation

typealias FavouriteMovieIds = [MovieId]

extension UserDefaults {

    struct Keys {
        static let favouriteMovies = "favouritesMovieIDArray"
    }

    class var favourites: FavouriteMovieIds {
        let storedValue = UserDefaults.standard.array(forKey: UserDefaults.Keys.favouriteMovies)
        guard let favourites = storedValue as? FavouriteMovieIds else { return [] }
        return favourites
    }

    class func set(favouriteMovies: FavouriteMovieIds) {
        UserDefaults.standard.set(favouriteMovies, forKey: UserDefaults.Keys.favouriteMovies)
    }

}

class Favourites {

    var movies: FavouriteMovieIds {
        guard let favourites = UserDefaults.standard.value(forKey: UserDefaults.Keys.favouriteMovies) as? FavouriteMovieIds else { return [] }
        return favourites
    }

    func add(movieId: MovieId) {
        var favouriteMovies = self.movies
        if !isFavourite(movieId: movieId) {
            favouriteMovies.append(movieId)
            persistFavouriteMovies(favouriteMovies: favouriteMovies)
        }
    }

    func remove(movieId: MovieId) {
        var favouriteMovies = self.movies
        guard let indexToRemove = favouriteMovies.firstIndex(of: movieId) else { return }
        favouriteMovies.remove(at: indexToRemove)
        persistFavouriteMovies(favouriteMovies: favouriteMovies)
    }

    func clearFavouriteMovies() {
        UserDefaults.standard.removeObject(forKey: UserDefaults.Keys.favouriteMovies)
    }


    func isFavourite(movieId: MovieId) -> Bool {
        guard let _ = movies.firstIndex(of: movieId) else { return false }
        return true
    }

    func persistFavouriteMovies(favouriteMovies: FavouriteMovieIds) {
        UserDefaults.set(favouriteMovies: favouriteMovies)
    }

}
