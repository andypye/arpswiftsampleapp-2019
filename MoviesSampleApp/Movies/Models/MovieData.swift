//
//  MovieData.swift
//  PremierSwift
//
//  Created by Andy on 26/06/2019.
//  Copyright © 2019 Pyrotechnic Apps. All rights reserved.
//
import Foundation

struct MovieData: Decodable {
    var page: Int
    var totalResults: Int
    var totalPages: Int
    var movies: [Movie]
    var favourites = Favourites()
    var dateFormatter = DateFormatter()

    enum CodingKeys: String, CodingKey {
        case results
        case page
        case totalResults = "total_results"
        case totalPages = "total_pages"
    }

    init(from decoder: Decoder) throws {
    let values = try decoder.container(keyedBy: CodingKeys.self)
        do {
            let decoder = JSONDecoder()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            decoder.dateDecodingStrategy = .formatted(dateFormatter)
            page = try values.decode(Int.self, forKey: .page)
            totalResults =  try values.decode(Int.self, forKey: .totalResults)
            totalPages =  try values.decode(Int.self, forKey: .totalPages)
            movies = try values.decode([Movie].self, forKey: .results)
        }
        catch let error {
            print("ERROR: Movie Data JSON decode failed: \(error)")
            throw error
        }
    }
}
