//
//  MovieDelegate.swift
//  PremierSwift2
//
//  Created by Andy on 30/06/2019.
//  Copyright © 2019 Pyrotechnic Apps. All rights reserved.
//

import Foundation

protocol MovieDelegate {
    var movieData: MovieData? { get }
    func didAddFavourite(movie: Movie)
    func didRemoveFavourite(movie: Movie)
}
