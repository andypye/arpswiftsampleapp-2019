//
//  APIEndPoint.swift
//  PremierSwift
//
//  Created by Andy on 26/06/2019.
//  Copyright © 2019 Pyrotechnic Apps. All rights reserved.
//

import Foundation

enum APIEndpoint {
    // MARK: - Definition
    case topRatedMovies(apiKey: String)
    case images(imagePath: String)

    // MARK: - Computed properties
    var scheme: String {
        return "https"
    }

    var host: String {
        switch self {
        case .topRatedMovies:
            return "api.themoviedb.org"
        case .images:
            return "image.tmdb.org"
        }
    }

    var path: String {
        switch self {
        case .topRatedMovies:
            return "/3/movie/top_rated"
        case let .images(imagePath):
            return "/t/p/w500\(imagePath)"
        }
    }

    var fullPath: String {
        return "\(self.scheme)://\(self.host)\(self.path)"
    }

    var urlQueryItems: [URLQueryItem] {
        switch self {
        case let .topRatedMovies(apiKey):
            let queryItems = [URLQueryItem(name: "api_key", value: apiKey),]
            return queryItems
        case .images:
            return []
        }
    }
    
    var httpMethod: String {
        switch self {
        case .topRatedMovies:
            return "GET"
        case .images:
            return "GET"
        }
    }

    var contentType: String {
        switch self {
        case .topRatedMovies:
            return "application/json"
        case .images:
            return "application/json"
        }
    }

    var request: URLRequest? {
        var urlComponents = URLComponents()
        urlComponents.scheme = self.scheme
        urlComponents.host =  self.host
        urlComponents.path = self.path
        urlComponents.queryItems = self.urlQueryItems

        guard let url = urlComponents.url else {
            print("ERROR: Could not create movie URL from components")
            return nil
        }
        var request = URLRequest(url: url)
        request.httpMethod = self.httpMethod
        var headers = request.allHTTPHeaderFields ?? [:]
        headers["Content-Type"] = self.contentType
        request.allHTTPHeaderFields = headers
        return request
    }

}
