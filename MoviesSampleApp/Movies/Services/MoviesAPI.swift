//
//  MoviesAPI.swift
//  PremierSwift
//
//  Created by Andy on 26/06/2019.
//  Copyright © 2019 Pyrotechnic Apps. All rights reserved.
//
import Foundation
struct MoviesAPI {

    // MARK: - Enums
    private enum HttpSuccess {
        case success
        case fail
    }

    typealias getMovieCompletionHandler = ((MovieData?, _ success: Bool, _ message: String) -> ())

    //MARK: - Static methods
    static func getMovies(completion: @escaping getMovieCompletionHandler) {
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        let apiKey = "e4f9e61f6ffd66639d33d3dde7e3159b" // TODO: remove from code base

        guard let request = APIEndpoint.topRatedMovies(apiKey: apiKey).request else {
            print("ERROR: the request could not be created")
            return
        }
        let task = session.dataTask(with: request, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) -> Void in
            guard let statusCode = getStatusCode(response: response) else {
                completion(nil, false, "Status code was nil, error: \(String(describing: error))")
                return
            }
            guard validateRequestStatus(statusCode: statusCode) == .success else {
                completion(nil, false, "Unable to fetch Movies, status code \(statusCode)")
                return
            }
            if let error = error {
                // Failure
                self.logFailure(response: response, error: error)
                completion(nil, false, "Unable to fetch Movies, please try again")
            } else {
                // Success
                guard let movies = parseMovieJSON(data: data, response: response) else {
                    completion(nil, false, "Unable to fetch Movies, please try again")
                    return
                }
                completion(movies, true, "Fetch Movies successful was all good")
            }
        })
        task.resume()
    }

    static func parseMovieJSON(data: Data?, response: URLResponse?) -> MovieData? {
        guard let jsonData = data else {
            print("ERROR: guard let jsonData failed")
            return nil
        }

        do {
            let jsonDecoder = JSONDecoder()
            let movieData = try jsonDecoder.decode(MovieData.self, from: jsonData)
            return movieData
        }
        catch let error {
            print("ERROR: JSON decode failed error: \(error)")
            return nil
        }
    }

    static private func getStatusCode(response: URLResponse?) -> Int? {
        guard let httpUrlResponse = response as? HTTPURLResponse else { return nil }
        return httpUrlResponse.statusCode
    }

    static private func getStatusCodeString(response: URLResponse?) -> String {
        if let getStatusCode = getStatusCode(response: response) {
            return String(getStatusCode)
        }
        else {
            return "Unknown"
        }
    }

    static private func validateRequestStatus(statusCode: Int?) -> HttpSuccess {
        // 5xx Server errors, 4xx Client errors, 3xx Redirection, 2xx Success
        if let statusCode = statusCode, statusCode < 300 {
            return HttpSuccess.success
        }
        else {
            return HttpSuccess.fail
        }
    }

    static private func logFailure(response: URLResponse?, error: Error) {
        print("ERROR: Fetch of Feed failed wth code:\(getStatusCodeString(response: response))\n\nerror: \(error)")
    }

}
