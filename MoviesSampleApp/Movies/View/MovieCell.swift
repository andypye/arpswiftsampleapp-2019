//
//  MovieCell.swift
//  PremierSwift
//
//  Created by Andy on 26/06/2019.
//  Copyright © 2019 Pyrotechnic Apps. All rights reserved.
//

import UIKit

class MovieCell: UITableViewCell {

    // MARK: - IBOutlets
    @IBOutlet weak var thumbnailImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!

    override func awakeFromNib() {
        titleLabel.text = ""
        overviewLabel.text = ""
        titleLabel.textColor = .titleText
        titleLabel.font = .title
        overviewLabel.textColor = .bodyText
        overviewLabel.font = .body
        thumbnailImage.setPlaceHolder()
    }

    func configure(withViewModel viewModel: MovieCellViewModel) {
        titleLabel.text = viewModel.title
        overviewLabel.text = viewModel.overview
        thumbnailImage.loadImageUsingUrlString(urlString: viewModel.imageFullPath
            ,imageCache: viewModel.imageCache)
    }

}
